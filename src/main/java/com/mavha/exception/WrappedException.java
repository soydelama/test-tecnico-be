package com.mavha.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception definida como 'salida' para todos los casos.
 * @author ariel
 *
 */

@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
public class WrappedException extends RuntimeException {

	private static final long serialVersionUID = -5243083775380838181L;

	public WrappedException(String message) {
		super(message);
	}

}
