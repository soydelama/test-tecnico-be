package com.mavha.component;

public class Constantes {

	public static final String FOLDER_UPLOADS = "uploads";
	
	public static final String PATH_UPLOADS = "./" + FOLDER_UPLOADS + "/";

	public static final String URL_UPLOADS = "http://localhost:8080/" + FOLDER_UPLOADS + "/";

}
