package com.mavha.component;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mavha.domain.Estado;
import com.mavha.domain.enums.EstadoEnum;
import com.mavha.service.EstadoService;

@Component
public class InicializarEstados {

		// atributos
	@Autowired
	private EstadoService estadoService;

		// metodos
	@PostConstruct
	public void iniciar() {
		this.estadoService.add(new Estado(EstadoEnum.PENDIENTE.getId(), EstadoEnum.PENDIENTE.getDescripcion()));
		this.estadoService.add(new Estado(EstadoEnum.RESUELTO.getId(), EstadoEnum.RESUELTO.getDescripcion()));
	}

		// gets y sets
	public EstadoService getEstadoService() {
		return estadoService;
	}

	public void setEstadoService(EstadoService estadoService) {
		this.estadoService = estadoService;
	}

}
