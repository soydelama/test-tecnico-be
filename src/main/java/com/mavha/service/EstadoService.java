package com.mavha.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mavha.domain.Estado;
import com.mavha.exception.WrappedException;
import com.mavha.repository.EstadoRepository;

@Service
public class EstadoService {

		// atributos
	@Autowired
	private EstadoRepository estadoRepository;

		// metodos
	public Estado add(Estado estado) {
		return this.estadoRepository.save(estado);
	}

	public Estado findById(Integer id) {
		return this.estadoRepository.findById(id).orElse(null);
	}

	public void delete(Integer id) {
		Estado estado = this.estadoRepository.findById(id).orElse(null);
		if (estado!=null) {
			this.estadoRepository.delete(estado);
		}
	}

	public List<Estado> getAll() {
		try {
			List<Estado> estados = new ArrayList<Estado>();
			this.estadoRepository.findAll().forEach( e -> estados.add(e));
			return estados;
		} catch (Exception e) {
			e.printStackTrace();
			throw new WrappedException(e.getMessage());
		}
	}

}
