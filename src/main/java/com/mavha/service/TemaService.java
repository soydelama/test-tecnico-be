package com.mavha.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mavha.domain.Estado;
import com.mavha.domain.Tema;
import com.mavha.domain.enums.EstadoEnum;
import com.mavha.exception.WrappedException;
import com.mavha.repository.EstadoRepository;
import com.mavha.repository.TemaRepository;
import com.mavha.repository.TemaRepositoryQueries;

@Service
public class TemaService {

		// atributos
	@Autowired
	private TemaRepository temaRepository;

	@Autowired
	private EstadoRepository estadoRepository;

	@Autowired
	private TemaRepositoryQueries temaRepositoryQueries;

		// metodos
	public List<Tema> getAll() {
		try {
			List<Tema> temas = new ArrayList<Tema>();
			this.temaRepository.findAll().forEach( t -> temas.add(t));
			return temas;
		} catch (Exception e) {
			e.printStackTrace();
			throw new WrappedException(e.getMessage());
		}
	}

	@Transactional
	public Tema add(Tema tema) {
		try {
			if (tema.getEstado()==null) {
				Estado estadoPendiente = this.estadoRepository.findById(EstadoEnum.PENDIENTE.getId()).get();
				tema.setEstado(estadoPendiente);
			}
			tema.refactNombreDeArchivo();
			return this.temaRepository.save(tema);
		} catch (Exception e) {
			e.printStackTrace();
			throw new WrappedException(e.getMessage());
		}
	}

	@Transactional
	public void cambiarEstado(Integer id) {
		try {
			Tema tema = this.temaRepository.findById(id).get();
			Estado estadoResuelto = this.estadoRepository.findById(EstadoEnum.RESUELTO.getId()).get();
			tema.setEstado(estadoResuelto);
			this.temaRepository.save(tema);
		} catch (Exception e) {
			e.printStackTrace();
			throw new WrappedException(e.getMessage());
		}
	}

	public List<Tema> getFiltered(String descripcionTema, Integer idEstado) {
		try {
			return this.temaRepositoryQueries.getFiltered(descripcionTema, idEstado);	
		} catch (Exception e) {
			e.printStackTrace();
			throw new WrappedException(e.getMessage());
		}
	}

	public Tema getById(Integer id) {
		try {
			Optional<Tema> opTema = this.temaRepository.findById(id);
			if (!opTema.isPresent()) {
				throw new Exception("Tema inseperado");
			}
			return opTema.get(); 	
		} catch (Exception e) {
			e.printStackTrace();
			throw new WrappedException(e.getMessage());
		}
	}

	public void delete(Integer id) {
		Tema tema = this.temaRepository.findById(id).orElse(null);
		if (tema!=null) {
			this.temaRepository.delete(tema);
		}
	}

		// gets y sets
	public TemaRepository getTemaRepository() {
		return temaRepository;
	}

	public void setTemaRepository(TemaRepository temaRepository) {
		this.temaRepository = temaRepository;
	}

	public EstadoRepository getEstadoRepository() {
		return estadoRepository;
	}

	public void setEstadoRepository(EstadoRepository estadoRepository) {
		this.estadoRepository = estadoRepository;
	}

	public TemaRepositoryQueries getTemaRepositoryQueries() {
		return temaRepositoryQueries;
	}

	public void setTemaRepositoryQueries(TemaRepositoryQueries temaRepositoryQueries) {
		this.temaRepositoryQueries = temaRepositoryQueries;
	}

}
