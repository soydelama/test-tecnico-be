package com.mavha.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Estado {

		// atributos
	@Id
	private Integer id;
	private String descripcion;
	
		// constructores
	public Estado() {
		super();
	}
	
	public Estado(Integer id, String descripcion) {
		this();
		this.id = id;
		this.descripcion = descripcion;
	}
	
		// gets y sets
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
