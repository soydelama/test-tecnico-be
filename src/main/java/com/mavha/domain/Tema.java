package com.mavha.domain;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.mavha.component.Constantes;

@Entity
public class Tema {

		// atributos
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String descripcion;
	private String nombreDeArchivo;
	private String urlImage;

	@ManyToOne
	@JoinColumn(name="estado_id", foreignKey=@ForeignKey(name="estado_id_fk"))
	private Estado estado;

		// constructores
	public Tema() {
		super();
	}

	public Tema(String descripcion, String nombreDeArchivo, String urlImage, Estado estado) {
		this();
		this.descripcion = descripcion;
		this.nombreDeArchivo = nombreDeArchivo;
		this.urlImage = urlImage;
		this.estado = estado;
	}

		// metodos
	public void refactNombreDeArchivo() {
		if (this.nombreDeArchivo!=null) {
			this.nombreDeArchivo = this.nombreDeArchivo.substring(this.nombreDeArchivo.lastIndexOf("\\")+1);
			this.urlImage = Constantes.URL_UPLOADS + this.nombreDeArchivo;
		}
	}

		// gets y sets
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public String getNombreDeArchivo() {
		return nombreDeArchivo;
	}

	public void setNombreDeArchivo(String nombreDeArchivo) {
		this.nombreDeArchivo = nombreDeArchivo;
	}

	public String getUrlImage() {
		return urlImage;
	}

	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}

}
