package com.mavha.domain.enums;

public enum EstadoEnum {

	PENDIENTE(1, "PENDIENTE"), 
	RESUELTO(2, "RESUELTO");
	
	private Integer id;
	private String descripcion;
	
	EstadoEnum(Integer id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}

	public Integer getId() {
		return id;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
