package com.mavha;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HolaRest {

	@RequestMapping("/hola")
	public String holaMundo() {
		return "hola mundo!";
	}

}
