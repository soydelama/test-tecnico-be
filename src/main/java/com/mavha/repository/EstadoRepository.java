package com.mavha.repository;

import org.springframework.data.repository.CrudRepository;

import com.mavha.domain.Estado;

public interface EstadoRepository extends CrudRepository<Estado, Integer> {

}
