package com.mavha.repository;

import org.springframework.data.repository.CrudRepository;

import com.mavha.domain.Tema;

public interface TemaRepository extends CrudRepository<Tema, Integer> {

}
