package com.mavha.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mavha.domain.Estado;
import com.mavha.domain.Tema;

@Repository
public class TemaRepositoryQueries {

		// atributos
	private SessionFactory sessionFactory;

		// constructor
	@Autowired
	public TemaRepositoryQueries(EntityManagerFactory factory) {
		if(factory.unwrap(SessionFactory.class) == null){
			throw new NullPointerException("factory is not a hibernate factory");
		}
		this.sessionFactory = factory.unwrap(SessionFactory.class);
	}

		// metodos
	public List<Tema> getFiltered(String descripcion, Integer idEstado) {
		Session session = this.sessionFactory.openSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Tema> query = builder.createQuery(Tema.class);
		Root<Tema> root = query.from(Tema.class);
		query.select(root);
		List<Predicate> predicates = new ArrayList<Predicate>();
		if (StringUtils.isNotBlank(descripcion)) {
			Predicate predicateDescripcion = builder.equal(root.get("descripcion"), descripcion);
			predicates.add(predicateDescripcion);
		}
		if (idEstado!=null) {
			Join<Tema, Estado> estadoJoin = root.join("estado", JoinType.INNER);
			Predicate predicateEstado = builder.equal(estadoJoin.get("id"), idEstado);
			predicates.add(predicateEstado);
		}
		query.where(builder.and(predicates.toArray(new Predicate[] {})));
		List<Tema> temas = session.createQuery(query).getResultList();
		return temas;
	}

}
