package com.mavha.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mavha.domain.Estado;
import com.mavha.service.EstadoService;

@RestController
@RequestMapping("/estados")
public class EstadoController {

		// atributos
	@Autowired
	private EstadoService estadoService;
	
		// metodos
	@RequestMapping(method=RequestMethod.GET)
	@CrossOrigin(origins = "http://localhost:4200")
	public List<Estado> getAll() {
		return this.estadoService.getAll();
	}

}
