package com.mavha.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.mavha.component.Constantes;
import com.mavha.domain.Tema;
import com.mavha.service.TemaService;

@RestController
@RequestMapping("/temas")
public class TemaController {

		// atributos
	@Autowired
	private TemaService temaService;

		// metodos
	@RequestMapping(method=RequestMethod.GET)
	@CrossOrigin(origins = "http://localhost:4200")
	public List<Tema> getAll() {
		return this.temaService.getAll();
	}

	@RequestMapping(method=RequestMethod.POST)
	@CrossOrigin(origins = "http://localhost:4200")
	public void addTema(@RequestBody Tema tema) {
		this.temaService.add(tema);
	}

	@RequestMapping(value="/filtered", method=RequestMethod.GET)
	@CrossOrigin(origins = "http://localhost:4200")
	public List<Tema> getFiltered(@RequestParam Map<String, String> queryParameters) {
		String idTema = queryParameters.get("idTema");
		String descripcionTema = queryParameters.get("descripcionTema");
		String idEstado = queryParameters.get("idEstado");
		if (StringUtils.isNotBlank(idTema)) {
			List<Tema> temas = new ArrayList<Tema>();
			temas.add(this.temaService.getById(Integer.valueOf(idTema)));
			return temas;
		}
		Integer iidEstado = (StringUtils.isNotBlank(idEstado))?Integer.valueOf(idEstado):null;
		iidEstado = (iidEstado<0)?null:iidEstado;
		return this.temaService.getFiltered(descripcionTema, iidEstado);
	}

	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	@CrossOrigin(origins = "http://localhost:4200")
	public void updateEstado(@RequestBody Tema tema, @PathVariable Integer id) {
		this.temaService.cambiarEstado(id);
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	@CrossOrigin(origins = "http://localhost:4200")
	public void UploadFile(MultipartHttpServletRequest request) throws IOException {
		Iterator<String> itr = request.getFileNames();
		MultipartFile file = request.getFile(itr.next());
		String fileName = file.getOriginalFilename();
		File dir = new File(Constantes.PATH_UPLOADS);
		if (dir.isDirectory()) {
			File serverFile = new File(dir, fileName);
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
			stream.write(file.getBytes());
			stream.close();
		}
	}
	
		// gets y sets
	public TemaService getTemaService() {
		return temaService;
	}

	public void setTemaService(TemaService temaService) {
		this.temaService = temaService;
	}

}
