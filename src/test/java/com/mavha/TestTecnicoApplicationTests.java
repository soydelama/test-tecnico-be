package com.mavha;

import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.mavha.domain.Estado;
import com.mavha.domain.Tema;
import com.mavha.domain.enums.EstadoEnum;
import com.mavha.service.EstadoService;
import com.mavha.service.TemaService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestTecnicoApplicationTests {

	@Autowired
	private EstadoService estadoService;

	@Autowired
	private TemaService temaService;

	@Test
	public void contextLoads() {
	}

	@Test
	public void estadoCRUD() {
		Estado nuevoEstado = this.estadoService.add(new Estado(1, "Prueba"));
		Estado estadoConsultado = this.estadoService.findById(nuevoEstado.getId());
    	Assert.assertNotNull(estadoConsultado);
    	Assert.assertEquals(nuevoEstado.getDescripcion(), estadoConsultado.getDescripcion());
    	this.estadoService.delete(estadoConsultado.getId());
    	estadoConsultado = this.estadoService.findById(nuevoEstado.getId());
    	Assert.assertNull(estadoConsultado);
    }

	@Test
	public void temaCRUD() {
    	Estado pendiente = this.estadoService.add(new Estado(EstadoEnum.PENDIENTE.getId(), EstadoEnum.PENDIENTE.getDescripcion()));
    	Estado resuelto = this.estadoService.add(new Estado(EstadoEnum.RESUELTO.getId(), EstadoEnum.RESUELTO.getDescripcion()));
    	Tema tema1 = this.temaService.add(new Tema("prueba 1", "archivo.png", "url", pendiente));
    	Tema tema2 = this.temaService.add(new Tema("prueba 1", "archivo.png", "url", pendiente));
    	Collection<Tema> temas = this.temaService.getAll();
    	Assert.assertEquals(2, temas.size());
    	this.temaService.cambiarEstado(tema1.getId());
    	tema1 = this.temaService.getById(tema1.getId());
    	Assert.assertEquals(resuelto.getId(), tema1.getEstado().getId());
    	this.temaService.delete(tema1.getId());
    	this.temaService.delete(tema2.getId());
    	temas = this.temaService.getAll();
    	Assert.assertEquals(0, temas.size());
    }

	@Test
	public void temaQueries() {
    	Estado pendiente = this.estadoService.add(new Estado(EstadoEnum.PENDIENTE.getId(), EstadoEnum.PENDIENTE.getDescripcion()));
    	Estado resuelto = this.estadoService.add(new Estado(EstadoEnum.RESUELTO.getId(), EstadoEnum.RESUELTO.getDescripcion()));
    	Tema tema1 = this.temaService.add(new Tema("prueba 1", "archivo.png", "url", pendiente));
    	Tema tema2 = this.temaService.add(new Tema("prueba 1", "archivo.png", "url", resuelto));
    	Tema tema3 = this.temaService.add(new Tema("prueba 2", "archivo.png", "url", pendiente));
    	Collection<Tema> temas = this.temaService.getFiltered("prueba 1", null);
    	Assert.assertEquals(2, temas.size());
    	temas = this.temaService.getFiltered("prueba 1", pendiente.getId());
    	Assert.assertEquals(1, temas.size());
    	temas = this.temaService.getFiltered(null, pendiente.getId());
    	Assert.assertEquals(2, temas.size());
    	this.temaService.delete(tema1.getId());
    	this.temaService.delete(tema2.getId());
    	this.temaService.delete(tema3.getId());
    	temas = this.temaService.getAll();
    	Assert.assertEquals(0, temas.size());
    	this.estadoService.delete(pendiente.getId());
    	this.estadoService.delete(resuelto.getId());
    	pendiente = this.estadoService.findById(pendiente.getId());
    	Assert.assertNull(pendiente);
    	resuelto = this.estadoService.findById(resuelto.getId());
    	Assert.assertNull(resuelto);
    }

}
